fn get_digit_at_pos(number: u32, position: u32) -> u32 {
	number % 10u32.pow(position) / 10u32.pow(position-1)
}

fn special_double(digit: u32) -> u32 {
	let mut result = digit * 2;
	if result > 9 {
		result = result % 10 + result / 10;
	}
	result
}

pub fn alternate_double(input: u32) -> Vec<u32> {
	let mut recalc_vector: Vec<u32> = vec![];
	let mut position = 1;
	let mut nb_length = 1;
	
	// We need to get number length
	while input / 10_u32.pow(nb_length) > 0 {
		nb_length += 1;
	}
	
	while position <= nb_length {
		let mut valeur = get_digit_at_pos(input, position);
		if position & 1 == 0 {
			// On est en position paire, on multiplie par 2
			valeur = special_double(valeur);
		}
		//println!("{}", valeur);
		recalc_vector.push(valeur);
		//recalculated_value += valeur * 10u32.pow(position-1);
		position += 1;
	}
	recalc_vector.reverse();
	recalc_vector
}
pub fn control_sum(alt_doubled: Vec<u32>) -> u32 {
	let mut recalculated_value: u32 = 0;
	
	for digit in alt_doubled {
		recalculated_value += digit;
	}
	recalculated_value
}

pub fn is_valid_identifier(identifier: u32) -> bool {
	let csum = control_sum(alternate_double(identifier));
	if csum % 10 == 0 {
		true
	} else {
		false
	}
}

// Some infos on testing : https://stackoverflow.com/questions/38995892/how-to-move-tests-into-a-separate-file-for-binaries-in-rusts-cargo
#[cfg(test)]
mod test {
	use super::*;
	#[test]
	fn test_alternate_double() {
		assert_eq!(alternate_double(8763), vec![7, 7, 3, 3]);
	}
	
	#[test]
	fn test_control_sum() {
		assert_eq!(control_sum(vec![7,7,3,3]), 20);
	}
	
	#[test]
	fn test_valid_id() {
		assert_eq!(is_valid_identifier(8763), true);
	}
	#[test]
	fn test_invalid_id() {
		assert_eq!(is_valid_identifier(8764), false);
	}
	
}
