//use std::env;
mod luhn;
// cf https://fr.wikipedia.org/wiki/Formule_de_Luhn


fn main() {
	let args: Vec<String> = std::env::args().collect();
	let user_value = args[1][..].parse::<u32>();
	let mut control_sum: u32 = 0;
	match user_value {
		Ok(number) => control_sum = luhn::control_sum(luhn::alternate_double(number)),
		Err(_) => panic!("Error, you did not provided a number")
	};
	println!("{}", control_sum);
    //new_value("8763".parse::<u32>().unwrap());
    //new_value(1111);
    //println!("{}", 3 & 1);
}
